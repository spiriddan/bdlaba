namespace Core;

internal enum MenuAnalyticE
{
    Tickets = 1,
    UniqueVisits = 2,
    AvgDiscount = 3
}

public class Analytic
{
    private const string Menu = "----\nAnalytics:\n" +
                                "1. How many tickets were sold to exhibition\n" +
                                "2. How many unique exhibitions visitor visited\n" +
                                "3. Average exhibition discount";
    
    
    private DatabaseContext.DatabaseContext context;

    public Analytic(DatabaseContext.DatabaseContext c)
    {
        context = c;
    }

    public void Go()
    {
        Console.WriteLine(Menu);
        int command;
        var ok = int.TryParse(Console.ReadLine(), out command);
        if (!ok)
        {
            Console.WriteLine("Input number!!");
            return;
        }

        switch (command)
        {
            case (int)MenuAnalyticE.Tickets:
                ultraHandler("exhibition", "Result: ", context.TicketsToExhibition);
                break;
            case (int)MenuAnalyticE.UniqueVisits:
                ultraHandler("visitor", "Result: ", context.VisitorVisited);
                break;
            case (int)MenuAnalyticE.AvgDiscount:
                ultraHandler("exhibition", "Result: ", context.AvgExhibitionDiscount);
                break;
            default:
                Console.WriteLine("dude");
                break;
        }
    }

    private void ultraHandler(string whosId, string outputString, Func<int, double> action)
    {
        Console.WriteLine($"Input {whosId}'s id: ");
        int id;
        var ok = int.TryParse(Console.ReadLine(), out id);
        if (!ok)
        {
            Console.WriteLine("dude...");
            return;
        }

        Console.WriteLine(outputString + action(id));
    }
    
}