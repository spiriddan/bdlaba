﻿using DatabaseModels;

namespace Core;

internal enum MenuE
{
    CRUD = 1,
    Analytic = 2,
    Exit = 3
}

internal enum Entities
{
    Visitor = 1,
    Exhibition = 2,
    Ticket = 3
}

public class Core
{
    private const string Menu = "-----\nMenu:\n" +
                                "1. Use entity CRUD\n" +
                                "2. Check analytic\n" +
                                "3. Exit";
    
    DatabaseContext.DatabaseContext context = new();
    private CRUDHandler CrudHandler;
    private Analytic Analytic;
    
    public Core()
    {
        CrudHandler = new CRUDHandler(context);
        Analytic = new Analytic(context);
    }
    
    
    public void Run()
    {
        Console.WriteLine("Here we go");
        bool flag = true;
        while (flag)
        {
            Console.WriteLine(Menu);
            int command;
            var ok = int.TryParse(Console.ReadLine(), out command);
            if (!ok)
            {
                Console.WriteLine("Input number!");
                continue;
            }

            switch (command)
            {
                case (int)MenuE.CRUD:
                    CrudHandler.CRUD();
                    break;
                case (int)MenuE.Analytic:
                    Analytic.Go();
                    break;
                case (int)MenuE.Exit:
                    Console.WriteLine("Bye");
                    flag = false;
                    break;
            }
        }
    }
}