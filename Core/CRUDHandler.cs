namespace Core;

internal enum CrudE{
    Create = 1,
    Read = 2,
    Update = 3,
    Delete = 4,
}

public class CRUDHandler
{
    private const string MenuCrud = "----\nCRUD:\n" +
                                    "1. Create\n" +
                                    "2. Read\n" +
                                    "3. Update\n" +
                                    "4. Delete\n" +
                                    "5. I changed my mind, bring me back";

    private const string MenuEntities = "----\nEntities:\n" +
                                        "1. Visitors\n" +
                                        "2. Exhibitions\n" +
                                        "3. Tickets\n" +
                                        "4. I changed my mind, bring me back";

    private DatabaseContext.DatabaseContext context;

    public CRUDHandler(DatabaseContext.DatabaseContext c)
    {
        context = c;
    }
    
    public void CRUD()
    {
        Console.WriteLine("Input entity number\n");
        Console.WriteLine(MenuEntities);
        
        int command;
        var ok = int.TryParse(Console.ReadLine(), out command);
        if (!ok)
        {
            Console.WriteLine("Input number!!");
            return;
        }

        switch (command)
        {
            case (int)Entities.Visitor:
                VisitorCommands();
                break;
            case (int)Entities.Exhibition:
                ExhibitionCommands();
                break;
            case (int)Entities.Ticket:
                TicketCommands();
                break;
            default:
                Console.WriteLine("dude...");
                break;
        }
    }
    
    private void VisitorCommands()
    {
        var id = -1;
        var name = "";
        var discount = -1;
        int action;
        Console.WriteLine("Input CRUD action:");
        Console.WriteLine(MenuCrud);
        var ok = int.TryParse(Console.ReadLine(), out action);
        if (!ok ||action < 1 ||action > 4)
        {
            Console.WriteLine("dude...");
            return;
        }
        
        if (action is (int)CrudE.Create or (int)CrudE.Update)
        {
            if(action is (int)CrudE.Update)
                Console.WriteLine("You can skip some points by entering default values (in brackets)");
            Console.WriteLine("Input visitor name (): ");
            name = Console.ReadLine();
            Console.WriteLine("Input visitor discount (-1): ");
            int.TryParse(Console.ReadLine(), out discount);
        }
        
        if(action != (int)CrudE.Create)
        {
            Console.WriteLine("Input visitor id: ");
            ok = int.TryParse(Console.ReadLine(), out id);
            if (!ok)
            {
                Console.WriteLine("dude...");
                return;
            }
        }

        VisitorCRUDHanlde(action, id, name, discount);
    }
    
    private void VisitorCRUDHanlde(int operation, int id = -1, string name = "", int discount = -1)
    {
        switch (operation)
        {
            case (int)CrudE.Create:
                context.AddVisitor(name, discount);
                break;
            case (int)CrudE.Read:
                var vis = context.GetVisitorById(id);
                if(vis == null)
                    return;
                Console.WriteLine(vis.ToString());
                break;
            case (int)CrudE.Update:
                context.UpdateVisitor(id, name:name, discount:discount);
                break;
            case (int)CrudE.Delete:
                context.DeleteVisitor(id);
                break;
            default:
                return;
        }

        context.SaveChanges();
    }
    
    private void ExhibitionCommands()
    {
        var id = -1;
        var name = "";
        DateTime date = default;
        int action;
        Console.WriteLine("Input CRUD action:");
        Console.WriteLine(MenuCrud);
        var ok = int.TryParse(Console.ReadLine(), out action);
        if (!ok ||action < 1 ||action > 4)
        {
            Console.WriteLine("dude...");
            return;
        }
        
        if (action is (int)CrudE.Create or (int)CrudE.Update)
        {
            if(action is (int)CrudE.Update)
                Console.WriteLine("You can skip some points by entering default values (in brackets)");
            Console.WriteLine("Input exhibition name (): ");
            name = Console.ReadLine();
            Console.WriteLine("Input exhibition date in format YYYY-MM-DD(): ");
            ok = DateTime.TryParse(Console.ReadLine(), out date);
            if (!ok)
            {
                date = DateTime.UtcNow;
            }
        }
        
        if(action != (int)CrudE.Create)
        {
            Console.WriteLine("Input exhibition id: ");
            ok = int.TryParse(Console.ReadLine(), out id);
            if (!ok)
            {
                Console.WriteLine("dude...");
                return;
            }
        }

        ExhibitionCRUDHanlde(action, id, name, date);
    }
    
    private void ExhibitionCRUDHanlde(int operation, int id = -1, string name = "", DateTime date = default)
    {
        switch (operation)
        {
            case (int)CrudE.Create:
                context.AddExhibition(name: name, date: date);
                break;
            case (int)CrudE.Read:
                var exh = context.GetExhibitionById(id);
                if (exh == null) 
                    return;
                Console.WriteLine(exh.ToString());
                break;
            case (int)CrudE.Update:
                context.UpdateExhibition(id, name:name, date:date);
                break;
            case (int)CrudE.Delete:
                context.DeleteExhibition(id);
                break;
            default:
                return;
        }
        context.SaveChanges();
    }
    
    private void TicketCommands()
    {
        var id = -1;
        var visitorId = -1;
        var exhibitionId = -1;
        var price = -1.0;
        int action;
        Console.WriteLine("Input CRUD action:");
        Console.WriteLine(MenuCrud);
        var ok = int.TryParse(Console.ReadLine(), out action);
        if (!ok ||action < 1 ||action > 4)
        {
            Console.WriteLine("dude...");
            return;
        }
        
        if (action is (int)CrudE.Create or (int)CrudE.Update)
        {
            if(action is (int)CrudE.Update)
                Console.WriteLine("You can skip some points by entering default values (in brackets)");
            Console.WriteLine("Input exhibition id (-1): ");
            int.TryParse(Console.ReadLine(), out exhibitionId);
            Console.WriteLine("Input visitor id (-1): ");
            int.TryParse(Console.ReadLine(), out visitorId);
            Console.WriteLine("Input ticket price (-1): ");
            double.TryParse(Console.ReadLine(), out price);
        }
        
        if(action != (int)CrudE.Create)
        {
            Console.WriteLine("Input ticket id: ");
            ok = int.TryParse(Console.ReadLine(), out id);
            if (!ok)
            {
                Console.WriteLine("dude...");
                return;
            }
        }

        TicketCRUDHanlde(action, id, visitorId, exhibitionId, price);
    }
    
    private void TicketCRUDHanlde(int operation, int id = -1, int visitorId = -1, 
                                            int exhibitionId = -1, double price = -1)
    {
        switch (operation)
        {
            case (int)CrudE.Create:
                context.AddTicket(visitorId, exhibitionId, price);
                break;
            case (int)CrudE.Read:
                var tick = context.GetTicketById(id);
                if(tick == null)
                    return;
                Console.WriteLine(tick.ToString());
                break;
            case (int)CrudE.Update:
                context.UpdateTicket(id:id, price:price, visitorId:visitorId, exhibitionId:exhibitionId);
                break;
            case (int)CrudE.Delete:
                context.DeleteTicket(id);
                break;
            default:
                return;
        }
        context.SaveChanges();
    }
}