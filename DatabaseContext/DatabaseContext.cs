﻿using DatabaseModels;
using Microsoft.EntityFrameworkCore;

namespace DatabaseContext;

public class DatabaseContext : DbContext
{
    private const string ConnectionString = "Host=localhost;Port=5432;Database=Laba;Username=postgres;Password=postgres";
    
    public DbSet<Exhibition> exhibitions { get; set; }
    public DbSet<Ticket> tickets { get; set; }
    public DbSet<Visitor> visitors { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Exhibition>().HasKey(x => x.Id);
        modelBuilder.Entity<Exhibition>().Property(x => x.Name).IsRequired();
        modelBuilder.Entity<Exhibition>().Property(x => x.Date).IsRequired();

        
        modelBuilder.Entity<Visitor>().HasKey(x => x.Id);
        modelBuilder.Entity<Visitor>().Property(x => x.Name).IsRequired();
        modelBuilder.Entity<Visitor>().Property(x => x.Discount).IsRequired();
        
        modelBuilder.Entity<Ticket>().HasKey(x => x.Id);
        modelBuilder.Entity<Ticket>().Property(x => x.Price).IsRequired();
        
        base.OnModelCreating(modelBuilder);
    }

    public DatabaseContext() : base(GetOptions(ConnectionString))
    {
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    }
    
    private static DbContextOptions GetOptions(string connectionString)
    {
        var optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
        optionsBuilder.UseNpgsql(connectionString);
        return optionsBuilder.Options;
    }
    
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(ConnectionString);
    }

    public void AddVisitor(string name, int discount)
    {
        visitors.Add(new Visitor(name, discount));
    }
    
    public void AddExhibition(string name, DateTime date)
    {
        exhibitions.Add(new Exhibition(name, date));
    }

    public void AddTicket(int visitorId, int exhibitionId, double price)
    {
        var vis = visitors.Find(visitorId);
        var exh = exhibitions.Find(exhibitionId);
        if (vis == null || exh == null)
        {
            Console.WriteLine($"Can't create ticket  {vis == null}   {exh == null}");
            return;
        }
        tickets.Add(new Ticket(price, visitorId, exhibitionId));
    }

    public Visitor? GetVisitorById(int id)
    {
        var res =  visitors.Find(id);
        if (res != null) return res;
        
        Console.WriteLine("Can't get visitor");
        return null;
    }
    
    public Exhibition? GetExhibitionById(int id)
    {
        var res =  exhibitions.Find(id);
        if (res != null) return res;
        
        Console.WriteLine("Can't get exhibition");
        return null;
    }
    
    public Ticket? GetTicketById(int id)
    {
        var res =  tickets.Find(id);
        if (res != null) return res;
        
        Console.WriteLine("Can't get ticket");
        return null;
    }

    public void DeleteTicket(int id)
    {
        var tick = tickets.Find(id); 
        if (tick == null)
        {
            Console.WriteLine("Can't find ticket to delete");
            return;
        }
        tickets.Remove(tick);
    }

    public void DeleteVisitor(int id)
    {
        var vis = visitors.Find(id);
        if (vis == null)
        {
            Console.WriteLine("Can't find visitor to delete");
            return;
        }
        var tick = tickets.Where(x => x.VisitorId == id);
        tickets.RemoveRange(tick);
        visitors.Remove(vis);
    }
    
    public void DeleteExhibition(int id)
    {
        var exh = exhibitions.Find(id);
        if (exh == null)
        {
            Console.WriteLine("Can't find exhibition to delete");
            return;
        }
        var tick = tickets.Where(x => x.ExhibitionId == id);
        tickets.RemoveRange(tick);
        exhibitions.Remove(exh);
    }

    public void UpdateVisitor(int id, int discount = -1, string name = "")
    {
        var vis = visitors.Find(id);
        if (vis == null)
        {
            Console.WriteLine("Can't find visitor to update");
            return;
        }

        if (discount != -1)
            vis.Discount = discount;
        if (name != "")
            vis.Name = name;
    }
    public void UpdateExhibition(int id, DateTime date = default, string name = "")
    {
        var exh = exhibitions.Find(id);
        if (exh == null)
        {
            Console.WriteLine("Can't find exhibition to update");
            return;
        }

        if (date != default)
            exh.Date = date;
        if (name != "")
            exh.Name = name;
    }
    
    public void UpdateTicket(int id, int visitorId = -1, int exhibitionId = -1, double price = default)
    {
        var ticket = tickets.Find(id);
        if (ticket == null)
        {
            Console.WriteLine("Can't find ticket to update");
            return;
        }

        if (visitorId != -1)
        {
            if (visitors.Find(visitorId) == null)
            {
                Console.WriteLine($"Can't find user with id {visitorId}");
                return;
            }
            ticket.VisitorId = visitorId;
        }
        
        if (exhibitionId != -1)
        {
            if (exhibitions.Find(exhibitionId) == null)
            {
                Console.WriteLine($"Can't find exhibition with id {exhibitionId}");
                return;
            }
            ticket.ExhibitionId = exhibitionId;
        }
        
        if (Math.Abs(price - (-1.0)) > 0)
            ticket.Price = price;
    }
    
    // Сколько билетов продано на выставку с указанным идентификатором
    public double TicketsToExhibition(int exhibitionId)
    {
        return tickets.Count(x => x.ExhibitionId == exhibitionId);
    }

    // На сколько уникальных выставок сходил посетитель с заданным идентификатором
    public double VisitorVisited(int visitorId)
    {
        return tickets.Where(x => x.VisitorId == visitorId).Select(x => x.ExhibitionId).Distinct().Count();
    }

    // Какой средний процент скидки (Discount) был у посетителей выставки с заданным идентификатором
    public double AvgExhibitionDiscount(int exhibitionId)
    {
        var ticks = tickets.Where(x => x.ExhibitionId == exhibitionId).Select(x => x.VisitorId);
        var vis =  visitors.Where(x => ticks.Contains(x.Id)).Select(x => x.Discount);
        if (!vis.Any())
            return 0;
        return vis.Average();
    }
}
