namespace DatabaseModels;

public class Ticket
{
    public int Id { get; set; }
    public int VisitorId { get; set; }
    public int ExhibitionId { get; set; }
    public double Price { get; set; }

    public Exhibition Exhibition { get; set; }
    public Visitor Visitor { get; set; }

    public Ticket(double price, int visitorId, int exhibitionId)
    {
        VisitorId = visitorId;
        ExhibitionId = exhibitionId;
        Price = price;
    }

    public override string ToString() => $"Id: {Id}\nVisitor id: {VisitorId}\n" +
                                         $"Exhibition id: {ExhibitionId}\nPrice: {Price}";
}