﻿#nullable enable
namespace DatabaseModels;

public class Visitor
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Discount { get; set; }
    
    public Visitor(string name, int discount)
    {
        Name = name;
        Discount = discount;
    }

    public override string ToString() => $"Id: {Id}\nName: {Name}\nDiscount: {Discount}";
}
