#nullable enable
namespace DatabaseModels;

public class Exhibition
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime Date { get; set; }


    public Exhibition(string name, DateTime date)
    {
        Name = name;
        Date = date;
    }

    public override string ToString() => $"Id: {Id}\nName: {Name}\nDate: {Date}";
}